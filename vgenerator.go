package vgenerator

import (
	"fmt"
	"math/rand"
	"reflect"
)

type SomeStructure struct {
	SomeString string
	SomeNumber int64
}

func GenValueByTypeKind(k reflect.Kind) interface{} {
	directlyOrAddress := func(v interface{}) interface{} {
		if rand.Int()%2 == 1 {
			return v
		} else {
			return &v
		}
	}

	switch k {
	case reflect.Bool:
		if rand.Int()%2 == 1 {
			return directlyOrAddress(true)
		} else {
			return directlyOrAddress(false)
		}
	case reflect.Int:
		return directlyOrAddress(rand.Int())
	case reflect.Uint64:
		return directlyOrAddress(rand.Uint64())
	case reflect.Float64:
		return directlyOrAddress(rand.Float64())
	case reflect.String:
		data := make([]byte, rand.Intn(20)+1)
		rand.Read(data)
		return string(data)
	case reflect.Array:
		switch rand.Intn(3) {
		case 0:
			return [3]int{rand.Int(), rand.Int(), rand.Int()}
		case 1:
			return [2]*SomeStructure{{"mice", 1}, {"cats", 1000}}
		case 2:
			return [4]interface{}{GenValueByTypeKind(reflect.Map), GenValueByTypeKind(reflect.Map)}
		}
		panic("a strange error")
	case reflect.Slice:
		slice := make([]SomeStructure, 0)
		for i := rand.Intn(30); i >= 0; i-- {
			slice = append(slice, SomeStructure{
				GenValueByTypeKind(reflect.String).(string),
				rand.Int63(),
			})
		}
		return slice
	case reflect.Map:
		m := make(map[string]interface{})
		for i := rand.Intn(4); i >= 0; i-- {
			m[GenValueByTypeKind(reflect.String).(string)] = GenRandomValue()
		}
		return m
	case reflect.Struct:
		structFields := make([]reflect.StructField, 0)
		for i := rand.Intn(4); i >= 0; i-- {
			v := GenRandomValue()
			structFields = append(structFields, reflect.StructField{
				Name: fmt.Sprintf("SF%X", rand.Int()),
				Type: reflect.TypeOf(v),
			})
		}
		return reflect.New(reflect.StructOf(structFields)).Interface()
	}
	return nil
}

func GenRandomTypeKind() reflect.Kind {
	dataTypesList := []reflect.Kind{
		reflect.Bool, reflect.Int, reflect.Uint64,
		reflect.Float64, reflect.String, reflect.Array,
		reflect.Slice, reflect.Map, reflect.Struct,
	}
	return dataTypesList[rand.Intn(len(dataTypesList))]
}

func GenRandomValue() interface{} {
	return GenValueByTypeKind(GenRandomTypeKind())
}