package vgenerator

import "testing"

func BenchmarkGenRandomValue(b *testing.B) {
	for i := 0; i < b.N; i++ {
		if GenRandomValue() == nil {
			b.Fatalf("GenRandomValue()=nil; want any value")
		}
	}
}