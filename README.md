# Random value generator

It is a very simple random value generator for Go.
It can generate values of following kinds:
reflect.Bool, reflect.Int, reflect.Uint64,
reflect.Float64, reflect.String, reflect.Array,
reflect.Slice, reflect.Map, reflect.Struct.